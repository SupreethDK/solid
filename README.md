# SOLID Design Principles

Solid principles are the software design principles in object-oriented software development intended to make software easy to read, understand, flexible and maintainable. The term **SOLID** is an [acronym](https://www.merriam-webster.com/dictionary/acronym) for five design principles.

Solid principles are developed by software engineer [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) and the theory of SOLID principles was introduced in his technical paper [_Design Principles and Design Patterns_](https://web.archive.org/web/20150906155800/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf). The SOLID acronym was introduced by Micheal Feathers.

## SOLID Acronym

- **S** - Single Responsibility Principle
- **O** - Open Closed Principle
- **L** - Liskov Substitution Principle
- **I** - Interface Segregation Principle
- **D** - Dependency Inversion Principle

For better understanding of these 5 principles, let us look at each principle with code examples. Please note that Javascript is used in code examples.

## Single Responsibility Principle

The [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) states that "class or module should have only one responsibility". In simple terms each class has single job or functionality in the software. Let us consider example using javascript,

```javascript
class PriceChecker {
  constructor(productPrice) {
    this.productPrice = productPrice;
    this.balance = 0;
  }

  balanceChecker(money) {
    this.balance += money;
    if (this.balance < this.productPrice) {
      this.printOutput();
    }
  }

  printOutput() {
    console.log("You don't have sufficient balance");
  }
}

let checkPrice = new PriceChecker(1000);
checkPrice.balanceChecker(450);
```

In above example we have determined a class called PriceChecker which consists two properties. It also has two functions called balanceChecker and printOutput. The objective of balanceChecker is to determine if the balance is less than the price of the product. If the balance is less than the printOutput function will print the message.

When we look at this example, this class clearly has two functionalities. As per the definition of single responsibility principle this code is not the right design. In order to make this code obey the rule, we have to move the printOutput function to different module and later import into the class PriceChecker. By doing this the different functionalities are not crammed into one class or module.

```javascript
// Module 1
export default functions printOutput(msg) {
    console.log(msg);
}
```

```javascript
// Module 2
import printOutput from "./file.js";
class PriceChecker {
  constructor(productPrice) {
    this.productPrice = productPrice;
    this.balance = 0;
  }

  balanceChecker(money) {
    this.balance += money;
    if (this.balance < this.productPrice) {
      printOutput("You don't have sufficient balance");
    }
  }
}
```

Now, class PriceChecker will have only reason to change and same with printFunction. This will make code easier to read and follow. Main advantage is better handling of fixing bug issues and refactoring of code will be easier.

## Open Closed Principle

The [open closed principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle) states that "objects or entities should be open for extension, but closed for modification". It means that it should be possible to add functionality to software application without breaking the existing code. Let us deep dive into this concept by considering an example which does not obey open closed principle,

```javascript
function printQuiz(questions) {
  questions.forEach((question) => {
    console.log(question.description);
    switch (question.type) {
      case "boolean":
        console.log("1. Correct");
        console.log("2. Wrong");
        break;
      case "multipleChoice":
        question.options.forEach((option, index) => {
          console.log(`${index + 1}. ${option}`);
        });
        break;
      case "text":
        console.log("Answer: _____");
        break;
    }
    console.log(" ");
  });
}

const questions = [
  {
    type: "boolean",
    description: "This is interesting topic",
  },
  {
    type: "multipleChoice",
    description: "What is your favorite food?",
    options: [" Dose", "Samosa", "Paratha", "Idly"],
  },
  {
    type: "text",
    description: "Describe your favorite food",
  },
];

printQuiz(questions);
```

The function printQuiz takes an array of questions and iterates over each of elements. The switch case statement is used check for the question type and give output based on it. The question is, what happens if we want to add new options or question type.

Considering that we have to add new question, we would have to add new case statement in order to recognize it and give output. This is there part where it breaks the closed principle. We should not make changes inside our function The printQuiz function should automatically work if we add new quiz type. The open portion is essentially saying that we have ability add inputs, in our case as question type. But printQuiz should not be changed inside.

Let's break this above code as per open closed principle, using classes:

We have classes for printing out different questions.

```javascript
class BooleanQuestion {
  constructor(description) {
    this.description = description;
  }
  printQuestionChoices() {
    console.log("1. Correct");
    console.log("2. Wrong");
  }
}

class MultipleChoiceQuestion {
  constructor(description, options) {
    this.description = description;
    this.options = options;
  }

  printQuestionChoices() {
    this.options.forEach((option, index) => {
      console.log(`${index + 1}. ${option}`);
    });
  }
}

class TextQuestion {
  constructor(description) {
    this.description = description;
  }
  printQuestionChoices() {
    console.log("Answer: _________");
  }
}
```

The printQuiz function prints out the question choice which is the function each one of our classes has. The list of questions is new version of those classes.

```javascript
function printQuiz(questions) {
    questions.forEach(question => {
        console.log(question.description)
        question.printQuestionChoices();
        console.log('');
    })
}

const questions = {
    new BooleanQuestion('This is interesting topic');
    new MultipleChoiceQuestion("What is your favorite food?",[" Dose", "Samosa", "Paratha", "Idly"]);
    new TextQuestion("Describe your favorite food")
}

printQuiz(questions);
```

Essentially what we have done is we have broken out the massive switch statement logic into individual classes that know exactly what to do with question type.

Now, all we do is add new question and create that corresponding class. Here we can observe that we never have to touch printQuiz function. It was closed to changes and open for extensibility.

In simple terms, open closed principle is saying that instead of changing code you want to create new code and that new code is going to work with your old code in order to do things. The open closed principle increases readability and makes easier to maintain code.

## Liskov Substitution Principle

[The Liskov Principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle) in object oriented programming states that "Functions that use pointers or references to base classes must be able to use objects of derived classes without knowing it."

![The Liskov Principle](https://i.stack.imgur.com/ilxzO.jpg)

Let say for example, we have class called animal. Every single place we use the "animal" class we should be able to replace it with one of its sub classes. If we have class called "dog" which inherits from animal then every single place we use animal we should be able to use "dog" and it should work just as before.

Let dig deeper into this by looking at javascript example,

```javascript
class Bird {
  fly() {
    console.log("I can fly");
  }
}

class Duck extends Bird {
  quack() {
    console.log("I can quack");
  }
}

class Peacock extends Bird {
  fly() {
    throw new Error("Cannot fly");
  }

  dance() {
    console.log("I am a dancer");
  }
}

function makeBirdFly(bird) {
  bird.fly();
}
```

Now we have three different classes. Our first is a base class called bird. Bird class has fly method assuming that all birds can fly. Next class is called as duck which extends bird class and has quack function since ducks quack. We have another class called as peacock class since peacock it extends bird class and peacocks cannot fly hence fly function will throw _error_ and there is another function called dance, since peacock loves to dance.

```javascript
const duck = new Duck();
const peacock = new Peacock();
```

We make two birds and let's run this code:

```javascript
makeBirdFly(duck);
makeBirdFly(peacock);
```

The duck prints out _"I can fly"_ but peacock throws error. This is where the Liskov principle comes, every subclass of class must be able to make this function work properly. The duck is passing Liskov principle but peacock is not passing. Peacock class cannot fly and it is failing its parent class. It is failing the Liskov Principle.

Let's refactor code such that it passes Liskov's principle.

```javascript
class FlyingBird {
  fly() {
    console.log("I can fly");
  }
}

class DancingBird {
  dance() {
    console.log("I can dance");
  }
}

class Duck extends FlyingBird {
  quack() {
    console.log("I can quack");
  }
}

class Peacock extends DancingBird {}

function makeFlyingBirdFly(bird) {
  bird.fly();
}

function makeDancingBirdDance(bird) {
  bird.dance();
}

const duck = new Duck();
const peacock = new Peacock();

makeFlyingBirdFly(duck);
makeDancingBirdDance(peacock);
```

When we run the above code, it returns _"I can fly"_ for duck and _"I can dance"_ for peacock. Hence passing the Liskov substitution principle because every subclass of DancingBird is properly able to call _makeDancingBirdDance_ function and same with subclass duck. Here code does not knows there is a subclass because that it needs to work as if it was parent class.

This code has problem inheritance in general. At last, all we need to know about Liskov substitution principle is that if you have a function that excepts class every single subclass of that parent class must also be able to enter that function and work properly.

## Interface Segregation Principle

As per Robert Martin [Interface Segregation Principle](https://en.wikipedia.org/wiki/Interface_segregation_principle) is _"Clients should not be forced to depend upon interfaces that they do not use."_

Since I am using Javascript in my code explanation, we should know that Javascript has no concept of [interface](https://stackoverflow.com/questions/2866987/what-is-the-definition-of-interface-in-object-oriented-programming). This principle is about segregating interface and making it smaller. In Javascript it can be done using [inheritance](https://stackoverflow.com/questions/3710275/does-javascript-have-the-interface-type-such-as-javas-interface) instead of using interfaces.

Let's look at example with pseudocode:

```
interface Entity {
	  attackDamage
	  health
	  name

	  move()
	  attack()
	  takeDamage(amount)
}


class Character implements Entity {

	  move() {
	    // Do something
    }

	  attack() {
	    // Do something
    }

  	takeDamage(amount) {
	    //Do something
    }
}

class Turret implements Entity{
	  move() {
	    //ERROR: CANNOT MOVE
    }
}
```

We have defined interface Entity with all properties and methods. Every single class that implements that
interface needs to define all of methods in that interface. But the problem is that when you have large interfaces which do lot's of different things.

In our example, Entity interface has properties and methods required for action-adventure video game. We have character which implements all of those. But if we consider turret class, it does not need to move. This does not make sense. As per this principle, when we have interface, you need everything that implements that interface to use every single portion of interface. Here turret class does not use move method, so we do not actually use all the parts of interface. Here, we are breaking the interface segregation principle. Solution is to break out larger interfaces to smaller components or interfaces.

## Dependency Inversion Principle

[Dependency Inversion Principle](https://en.wikipedia.org/wiki/Dependency_inversion_principle) says:

1. High level modules should not depend upon low-level modules. Both should depend upon abstractions.
2. Abstractions should never depend upon details. Details should depend upon abstractions.

In simple terms, Class should depend on abstractions (e.g interface, abstract classes), not specific details (implementations).

![Dependency Inversion Principle](https://i.stack.imgur.com/eOpu4.jpg)

Thoughts about above image: Every electrical source relies on an interface specific for the types of things that need to plug into it.

Let's look at example using Javascript, we have a class called _DownloadAndPrint_. In this class, we have the method: getDataFromAPI that downloads data from an external API and then print in the console. Here question arises in implementation to get data from specified end point, should we use Fetch api or do we want get get data from Axios or XMLHttpRequest.

If we decide to use fetch api like below example in all our classes, we will have to substitute one implementation for another in all classes where we are using it because we’re utilizing directly. Dependency Inversion Principle tells that we should use the interface, not the implementation.

```javascript
//Download.js
const url = "https://mountblue.tech/employee_list"
class Example {
 constructor() {
  ...
 }

 getDataFromAPI(params) {
  fetch(url)
  .then((res) => {
    if(res.ok === true) {
      return res.json()
    }
  })
  .then(data => console.table(data))
  .catch((error) => console.log(error));
 }
}
```

Let's try to implement Dependency Inversion Principle for same above example.

Since Javascript is not having interfaces, we are using module encapsulation which gives similar behavior. In below example, we will export a method that we will use to download dummy data. Here method uses Fetch API to get get data for given end point.

Now, let's create a _getData_ method in a middle.js file to encapsulate how to get the data.

```javascript
//getData.js
export const getData = (url) => {
  return fetch(url)
    .then((res) => {
      if (res.ok === true) {
        return res.json();
      }
    })
    .catch((error) => console.log(error));
};
```

Let's try using _getData_ dependency to download data by importing this module in _download.js_ and with slight modification we can achieve following:

```javascript
import { getData } from './getData.js'
const url = "https://mountblue.tech/employee_list"
class Example{
 constructor() {
  ...
 }
 downloadDataFromAPI(params) {
  getData(url)
    .then(data => console.table(data))
    .catch((error) => console.log(error));
 }
}
```

Now, if we want to switch to Axios, we will change the getData implementation in _getData.js_, and same if we want to use
XMLHttpRequest,

```javascript
export const getData = (url) => {
  return axios.get(url);
};
```

The Dependency Inversion Principle is the final design principle. It basically introduces an interface abstraction between software components to remove the dependencies between them.
